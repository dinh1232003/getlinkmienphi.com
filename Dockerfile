FROM amd64/alpine:3.17
WORKDIR /root/app
COPY . .
ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools
RUN pip3 install uvicorn
RUN pip3 install redis
RUN pip3 install fastapi
RUN python3 -m pip install requests
CMD ["uvicorn", "main:app", "--port", "80", "--host", "0.0.0.0"]