import requests

class Fshare:
    def __init__(self, email, password, proxy = ""):
        self.email = email
        self.password = password
        self.session = requests.Session()
        # if ()
        
        if proxy == "":
            self.proxy = {}
        else:
            self.proxy = {
            'http': 'http://' + proxy,
            'https': 'http://' + proxy
        }
        self.session.proxies = self.proxy
    def updateCookie(self, cookie):
        self.session.cookies.update(cookie)
    def getCookie(self):
        return self.session.cookies
    def checkLogin(self):
        response = self.session.get("https://fshare.vn/", verify=False)
        if response.url != 'https://www.fshare.vn/file/manager':
            return False
        return True
    def login(self):
        url = 'https://www.fshare.vn/site/login'
        response = self.session.post(url=url, verify=False, headers={
            'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
            'origin': 'https://www.fshare.vn',
            'referer': 'https://www.fshare.vn/',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"',
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-user': '?1', 
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9,vi;q=0.8',
            'cache-control': 'max-age=0',
            'content-type': 'application/x-www-form-urlencoded'
        }, data={
            'LoginForm[email]': self.email,
            'LoginForm[password]': self.password,
            'LoginForm[rememberMe]': '1',
            '_csrf-app': self.takeCsrf()
        })
        if response.url != 'https://www.fshare.vn/file/manager': 
            return {'status': 'error', 'code': 400, 'error': 'Login fail'}
        return {
            'status': 'success',
            'code': 200
        }
    def takeCsrf(self):
        response = self.session.get(url='https://www.fshare.vn/',verify=False, headers={
            'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"',
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-user': '?1', 
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9,vi;q=0.8',
            'cache-control': 'max-age=0'
        })
        text = response.text
        csrf = text.split('name="csrf-token" content="')[1].split('">')[0]
        return csrf
    def tranferOldPoint(self, email, number, levelTwoPassword, message = ''):
        response = self.session.post(url='https://www.fshare.vn/account/vip-fxu-point-management', verify=False, headers={
            'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
            'origin': 'https://www.fshare.vn',
            'referer': 'https://www.fshare.vn/account/vip-fxu-point-management',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"',
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-user': '?1', 
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9,vi;q=0.8',
            'cache-control': 'max-age=0',
            'content-type': 'application/x-www-form-urlencoded'
        }, data={
            '_csrf-app': self.takeCsrf(),
            'TransactionForm[password]': levelTwoPassword,
            'TransactionForm[unit]': '4',
            'TransactionForm[num]': number,
            'TransactionForm[email]': email,
            'TransactionForm[message]': message
        }, allow_redirects=True)
        if response.status_code == 302:
            return {
                'status': 'success',
                'code': 200
            }
        return {
            'status': 'error',
            'code': 400,
            'msg': 'Tranfer fail, pls check again'
        }
    def tranferNewPoint(self, email, number, levelTwoPassword, message = ''):
        response = self.session.post(url='https://www.fshare.vn/account/vip-fxu-point-management', verify=False, headers={
            'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
            'origin': 'https://www.fshare.vn',
            'referer': 'https://www.fshare.vn/account/vip-fxu-point-management',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"',
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-user': '?1', 
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9,vi;q=0.8',
            'cache-control': 'max-age=0',
            'content-type': 'application/x-www-form-urlencoded'
        }, data={
            '_csrf-app': self.takeCsrf(),
            'TransactionForm[password]': levelTwoPassword,
            'TransactionForm[unit]': '1',
            'TransactionForm[num]': number,
            'TransactionForm[email]': email,
            'TransactionForm[message]': message
        }, allow_redirects=True)
        if response.status_code == 302:
            return {
                'status': 'success',
                'code': 200
            }
        return {
            'status': 'error',
            'code': 400,
            'msg': 'Tranfer fail, pls check again'
        }
    def send_level_2_password(self, level_2_password):
        response = self.session.post(url='https://www.fshare.vn/account/level2-pass-input', headers={
            'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
            'origin': 'https://www.fshare.vn',
            'referer': 'https://www.fshare.vn/account/vip-fxu-point-management',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"',
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-user': '?1', 
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9,vi;q=0.8',
            'cache-control': 'max-age=0',
            'content-type': 'application/x-www-form-urlencoded',
            'x-csrf-token': self.takeCsrf(),
            'x-requested-with': 'XMLHttpRequest'
        }, data={
            '_csrf-app': self.takeCsrf(),
            'Level2PasswordForm[lvl2_pasword]': level_2_password
        })
    def upgradeVipOldPoint(self, day):
        response = self.session.post(url='https://www.fshare.vn/payment/checkout/order-fxu-point-payment', headers={
            'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
            'origin': 'https://www.fshare.vn',
            'referer': 'https://www.fshare.vn/payment/checkout/package/?package=7',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"',
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-user': '?1', 
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9,vi;q=0.8',
            'cache-control': 'max-age=0',
            'content-type': 'application/x-www-form-urlencoded',
            'x-csrf-token': self.takeCsrf(),
            'x-requested-with': 'XMLHttpRequest'
        }, data={
            'numDay': day,
            'type': 'old-point',
            'packageId': '7'
        })
        return {
            'data': response.json()
        }