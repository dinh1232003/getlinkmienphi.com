from pydantic import BaseModel
from fastapi import APIRouter
from .fshare import Fshare
import redis
import pickle
import requests

class Account(BaseModel):
    email: str
    password: str

class Tranfer(BaseModel):
    account: Account
    email_receive: str
    number: str
    message: str
    level2password: str 
    oldPoint: int
    proxy: str | None = ""

class UpgradeVip(BaseModel):
    account: Account
    vipDay: str
    proxy: str | None = ""

router = APIRouter()
redis_pool = redis.ConnectionPool(host='transfer_redis', port=6379)
redis_connect = redis.Redis(connection_pool=redis_pool)
@router.post("/tranfer")
def tranferOldPoint(tranfer: Tranfer):
    if tranfer.proxy == "":
        IP_PROXY = tranfer.proxy
    else:
        IP_PROXY = {}
        
    fshare = Fshare(email=tranfer.account.email, password=tranfer.account.password, proxy=IP_PROXY)
    """
    * Check had cookie from before login, if dont, login using password
    """
    if not redis_connect.exists(tranfer.account.email):
        login_status = fshare.login()
        if login_status['code'] == 400:
            return {'error': 'Login fail'}
    else:
        """
        * If had cookie from before login, check cookie live or die, if die, login using email password
        """
        fshare.updateCookie(pickle.loads(redis_connect.get(tranfer.account.email)))
        if not fshare.checkLogin():
            redis_connect.delete(tranfer.account.email)
            login_status = fshare.login()
            if login_status['code'] == 400:
                return {'error': 'Login fail'}
    fshare.send_level_2_password(tranfer.level2password)
    if tranfer.oldPoint == 0:
        tranfer_status = fshare.tranferNewPoint(email=tranfer.email_receive, number=tranfer.number, levelTwoPassword=tranfer.account.password, message=tranfer.message)
    else:
        tranfer_status = fshare.tranferOldPoint(email=tranfer.email_receive, number=tranfer.number, levelTwoPassword=tranfer.account.password, message=tranfer.message)
    redis_connect.set(tranfer.account.email, pickle.dumps(fshare.getCookie()))
    return 0

@router.post('/upgrade')
def upgrade(upgrade: UpgradeVip):
    # change_ip()
    if upgrade.proxy == "":
        IP_PROXY = ""
        # IP_PROXY = upgrade.proxy
    else:
        IP_PROXY = upgrade.proxy
        # IP_PROXY = {}
    fshare = Fshare(email=upgrade.account.email, password=upgrade.account.password, proxy=IP_PROXY)
    """
    * Check had cookie from before login, if dont, login using password
    """
    if not redis_connect.exists(upgrade.account.email):
        login_status = fshare.login()
        if login_status['code'] == 400:
            return {'error': 'Login fail'}
    else:
        """
        * If had cookie from before login, check cookie live or die, if die, login using email password
        """
        fshare.updateCookie(pickle.loads(redis_connect.get(upgrade.account.email)))
        if not fshare.checkLogin():
            redis_connect.delete(upgrade.account.email)
            login_status = fshare.login()
            if login_status['code'] == 400:
                return {'error': 'Login fail'}
    status = fshare.upgradeVipOldPoint(upgrade.vipDay)
    redis_connect.set(upgrade.account.email, pickle.dumps(fshare.getCookie()))
    return status

@router.get('/quote')
def get_quote(id: str = ''):
    if id == '':
        return {'error': 'id must be not null or empty'}
    # change_ip()
    # IP_PROXY = os.getenv('IP_PROXY')
    # print('https://www.fshare.vn/api/v3/tools/get-download-traffic-info?user_id={id_code}&verifyCode=cFWZJ%3Dy%21NK%40-k?63M%2AEy%2BL'.format(id_code = id))
    # return 0
    return requests.get('https://www.fshare.vn/api/v3/tools/get-download-traffic-info?user_id={id_code}&verifyCode=cFWZJ%3Dy%21NK%40-k?63M%2AEy%2BL'.format(id_code = id)).json()