# from typing import Union
from pydantic import BaseModel
from fastapi import FastAPI, status, Response, Request
import redis
import requests
import json
import secrets
import os
from Fshare import Fshare
from controller import transfer_and_upgrade

from fastapi.responses import JSONResponse, StreamingResponse
app = FastAPI()



app.include_router(transfer_and_upgrade.router)

api_redis = redis.Redis(host='api_redis', port=6379)
request_redis = redis.Redis(host='request_redis', port=6379)
download_redis = redis.Redis(host='download_redis', port=6379)

# r = redis.Redis(host='localhost', port=5505)

    # def __init__(self):
def loginAPI(email, password, proxy = {}):
    try:
        if (email == ""):
            return {"msg": "Username not empty"}
        if (password == ""):
            return {"msg": "Password not empty"}
        # return userName, passWord
        url = "https://api.fshare.vn/api/user/login"
        headers = {
            "Host": "api.fshare.vn",
            "Content-Type": "application/json; charset=UTF-8",
            "Connection": "keep-alive",
            "Accept": "*/*",
            "User-Agent": "FshareMacTool-062021",
            "Accept-Language": "en-VN;q=1.0",
            "Accept-Encoding": "br;q=1.0, gzip;q=0.9, deflate;q=0.8"
        }
        userInfo = {
            "user_email": email,
            "app_key": "6SMacqqgQQ2sTh9NR4gJgHF28mADpHCL",
            "password": password
        }
        response = requests.post(url=url, headers=headers, json=userInfo, proxies=proxy)
        if response.json()['msg'] != "Login successfully!":
            return False
        api_redis.set(email, value=json.dumps({'token': response.json()['token'], 'session_id': response.json()['session_id']}), ex=3600)
        return {'token': response.json()['token'], 'session_id': response.json()['session_id']}
    except:
        return False
    

def checkLogin(session_id, proxy = {}):
    url = "https://api.fshare.vn/api/user/get"
    header = {
        "Host": "api.fshare.vn",
        "Accept": "*/*",
        "Cookie": "session_id=" + session_id,
        "User-Agent": "Fshare-tool-macOs/1.0 (Fshare.Fshare-tool-macOs; build:1; macOS 12.6.1) Alamofire/5.4.3",
        "Accept-Language": "en-VN;q=1.0",
        "Accept-Encoding": "br;q=1.0, gzip;q=0.9, deflate;q=0.8",
        # "Content-Type": "application/json; charset=UTF-8",
        "Connection": "keep-alive",
    }
    response = requests.get(url=url, headers=header, proxies=proxy)
    # print(response.json())
    try:
        if response.json()['id'] != "":
            return True
    except:
        return False
        
def getLink(session, token, password, code, proxy = {}):
    url = "https://api.fshare.vn/api/session/download"
    # getlink.link = getlink.link.replace("/", "\/")
    header = {
        "Host": "api.fshare.vn",
        "Connection": "keep-alive",
        "Accept": "*/*",
        "Fshare-Session-id": session,
        "Accept-Language": "en-us",
        "User-Agent": "Fshare-tool-macOs/1 CFNetwork/1335.0.3 Darwin/21.6.0",
        "Content-Type": "application/json",
        "Accept-Encoding": "gzip, deflate, br"
    }
    data = {
        "token": token,
        "zipflag": "0",
        "password": password,
        "url": "https://fshare.vn/file/" + code
       
    }
    return requests.post(url=url, headers=header, json=data, proxies=proxy).json()

class Account(BaseModel):
    email: str
    password: str

class GetLink(BaseModel):
    account: Account
    file_code: str
    file_password: str | None = ""
    proxy: str | None = ""

@app.post('/get/api', status_code=200)
def handle(getlink: GetLink, response: Response):
    if getlink.proxy == '':
        proxy = {}
    else:
        proxy = {
            'http': 'http://' + getlink.proxy,
            'https': 'http://' + getlink.proxy
        }
    if not api_redis.exists(getlink.account.email):
        if not loginAPI(getlink.account.email, getlink.account.password, proxy):
            response.status_code = status.HTTP_401_UNAUTHORIZED
            return {"error": "Email or password not correct"}
    session = json.loads(api_redis.get(getlink.account.email))['session_id']
    token = json.loads(api_redis.get(getlink.account.email))['token']
    if not checkLogin(session, proxy):
        api_redis.delete(getlink.account.email)
        handle(getlink=getlink)
    location = getLink(session, token, getlink.file_password, getlink.file_code, proxy)
    try:
        response.status_code = 200
        token = secrets.token_hex(32)
        download_redis.set(name=token, value=json.dumps({'location': location['location'], 'proxy': proxy}))
        return {'location': '/dll/' + token}
        # return {'location': location['location']}
    except:
        response.status_code = 404
        return {'msg': location['msg']}



def get_link(token):
    if not download_redis.exists(token):
        return {
            'code': 400,
            'msg': 'Bad request'
        }
    data = json.loads(download_redis.get(token))
    return {
        'code': 200,
        'location': data['location']
    }

def stream_file(url, proxy = {}):
    with requests.get(url=url, stream=True, proxies=proxy) as r:
        r.raise_for_status()
        for chunk in r.iter_content(chunk_size=8129):
            yield chunk

def delete_token(token):
    download_redis.delete(token)

@app.get('/dll/{token}', status_code=200)
def download(token: str,response: Response):
    data = get_link(token=token)
    if data['code'] == 400:
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {'msg': data['msg']}
    file_name = data['location'].split("/")[-1]
    proxy = data['proxy']
    return StreamingResponse(stream_file(data['location'], proxy), headers={'Content-Disposition': 'attachment; filename="{file_name}"'.format(file_name=file_name)})
    

    
    # print(session)
    
    



# connect = "sqlite:///./account.db"
# database = Database(connect)

# connection = sqlite3.connect('accountList.db')
# @app.on_event("startup")
# async def connection():
#     await database.connect()
# @app.on_event("shutdown")
# async def disconnect():
#     await database.disconnect()





@app.post('/get/request')
async def getlink(getLink: GetLink):
    fshare = Fshare()
    if (not request_redis.exists(getLink.account.email)):
        if not fshare.login(getLink.account.email, getLink.account.password):
            return {"Error":"Email or password not correct"}
    if not fshare.update_cookie(getLink.account.email):
        if not fshare.login(getLink.account.email, getLink.account.password):
            return {"Error":"Email or password not correct"}
    # return True
    response = fshare.getlink(getLink.file_code)
    try:
        if (response['url'] != None): 
            token = secrets.token_hex(32)
            download_redis.set(name=token, value=json.dumps({'location': response['url']}))
            return {"token": token}
    except:
        return response

# class LoginInfo(BaseModel):
#     email: str | None = ""
#     password: str | None = ""


# class UserInfo(BaseModel):
#     session_id: str
#     token: str


# class getLinkFshare(BaseModel):
#     session_id: str
#     token: str
#     link: str
#     password: str | None = ""


# """
# * Param: Email and Password of Fshare account
# * Return: session_id and token of account

# """


# def loginFshareAccount(userLogin):
#     if (userLogin.email == ""):
#         return {"msg": "Username not empty"}
#     if (userLogin.password == ""):
#         return {"msg": "Password not empty"}
#     # return userName, passWord
#     url = "https://api.fshare.vn/api/user/login"
#     headers = {
#         "Host": "api.fshare.vn",
#         "Content-Type": "application/json; charset=UTF-8",
#         "Connection": "keep-alive",
#         "Accept": "*/*",
#         "User-Agent": "FshareMacTool-062021",
#         "Accept-Language": "en-VN;q=1.0",
#         "Accept-Encoding": "br;q=1.0, gzip;q=0.9, deflate;q=0.8"
#     }
#     userInfo = {
#         "user_email": userLogin.email,
#         "app_key": "6SMacqqgQQ2sTh9NR4gJgHF28mADpHCL",
#         "password": userLogin.password
#     }
#     return requests.post(url=url, headers=headers, json=userInfo).json()


# # def getInfoFshareAccount():
# #     pass


# # @app.get("/")
# # def read_root():
# #     return {"Hello": "World"}

# """
# * Param: userName and passWord
# * Return: session_id and token of account
# """
# @app.post("/login")
# def loginHandle(userLogin: LoginInfo):
#     return loginFshareAccount(userLogin)

# """
# * Param: session_id and token
# * Return: Infomation of Fshare account
# """
# @app.post("/info")
# def getInfoFshareAccount(loginAccount: UserInfo):
#     url = "https://api.fshare.vn/api/user/get"
#     header = {
#         "Host": "api.fshare.vn",
#         "Accept": "*/*",
#         "Cookie": "session_id=" + loginAccount.session_id,
#         "User-Agent": "Fshare-tool-macOs/1.0 (Fshare.Fshare-tool-macOs; build:1; macOS 12.6.1) Alamofire/5.4.3",
#         "Accept-Language": "en-VN;q=1.0",
#         "Accept-Encoding": "br;q=1.0, gzip;q=0.9, deflate;q=0.8",
#         # "Content-Type": "application/json; charset=UTF-8",
#         "Connection": "keep-alive",
#     }
#     return requests.get(url=url, headers=header).json()


# @app.post("/getlink")
# def getlinks(getlink: getLinkFshare):
#     url = "https://api.fshare.vn/api/session/download"
#     # getlink.link = getlink.link.replace("/", "\/")
#     header = {
#         "Host": "api.fshare.vn",
#         "Connection": "keep-alive",
#         "Accept": "*/*",
#         "Fshare-Session-id": getlink.session_id,
#         "Accept-Language": "en-us",
#         "User-Agent": "Fshare-tool-macOs/1 CFNetwork/1335.0.3 Darwin/21.6.0",
#         "Content-Type": "application/json",
#         "Accept-Encoding": "gzip, deflate, br"
#     }
#     data = {
#         "token": getlink.token,
#         "zipflag": "0",
#         "password": getlink.password,
#         "url": getlink.link
       
#     }
#     # return header, data
#     return requests.post(url=url, headers=header, json=data).json()
