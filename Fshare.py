import requests, pickle
import redis as r
# class Fshare:
#     def __init__(self,username, password):
#         self.username = username
#         self.password = password
#         self.session = requests.Session()
#         if (not(os.path.isfile(self.username + ".txt"))):
#             # password = input("Pass: ")
#             csrf = self.session.get('https://www.fshare.vn/').text
#             csrf = csrf.split('name="csrf-token" content="')[1]
#             csrf = csrf.split('">')[0]
#             headers = {
#                 'Content-Type': 'application/x-www-form-urlencoded',
#                 'Origin': 'https://www.fshare.vn',
#                 'Accept-Encoding': 'gzip, deflate, br',
#                 'Connection': 'keep-alive',
#                 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#                 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.2 Safari/605.1.15',
#                 'Referer': 'https://www.fshare.vn/',
#                 'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8'
#             }
#             data = {
#                 '_csrf-app': csrf,
#                 'LoginForm[email]': self.username,
#                 'LoginForm[password]': self.password,
#                 'LoginForm[rememberMe]': 0,
#                 'LoginForm[rememberMe]': 1
#             }
#             response = self.session.post('https://www.fshare.vn/site/login', headers=headers, data=data)
#             # response = self.session.get('https://www.fshare.vn/')
#             if response.url != "https://www.fshare.vn/file/manager" :
#                 if (os.path.isfile(self.username + ".txt")):
#                     os.remove(self.username + '.txt')
#                 raise Exception("Account error, try again")
#             with open(self.username + '.txt', 'wb+') as f:
#                 pickle.dump(self.session.cookies, f)
#         else:
#             with open(self.username + '.txt', 'rb') as f:
#                 try:
#                     self.session.cookies.update(pickle.load(f))
#                     response = self.session.get('https://www.fshare.vn/')
#                     if response.url != "https://www.fshare.vn/file/manager" :
#                         if (os.path.isfile(self.username + ".txt")):
#                             os.remove(self.username + '.txt')
#                         raise Exception("Cookie error, pls try again")
#                 except:
#                     os.remove(self.username + '.txt')
#                     raise Exception("Cookie error, pls try again")
#     def getCsrf(self,content):
#         return content.split('name="csrf-token" content="')[1].split('">')[0]
#     def getlink(self, link):
#         header = {
#             'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#             'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
#             'Accept-Encoding': 'gzip, deflate',
#             'Upgrade-Insecure-Requests': '1'
#         }
#         code = link.split('fshare.vn/file/')[1].split('?token')[0]
#         response = self.session.get(url=link, headers=header)
#         csrf = self.getCsrf(response.text)
#         header = {
#         # ':authority': 'www.fshare.vn',
#         # ':method': 'POST',
#         # ':path': '/download/get',
#         # ':scheme': 'https',
#         'Accept': '*/*',
#         'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
#         'Accept-Encoding': 'gzip, deflate, br',
#         'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8',
#         'Connection': 'keep-alive',
#         'Origin': 'https://www.fshare.vn',
#         'Referer': response.url,
#         'Sec-Fetch-Dest': 'empty',
#         'Sec-Fetch-Mode': 'cors',
#         'Sec-Fetch-Site': 'none',
#         'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1',
#         'X-CSRF-Token' : csrf,
#         'X-Requested-With': 'XMLHttpRequest',
#         # 'Cookie': cookie
#         }
#         data = {
#         '_csrf-app': csrf,
#         "linkcode": code,
#         "ushare": "",
#         "withFcode5": 0
#         }
#         return self.session.post('https://www.fshare.vn/download/get', headers=header, data=data).text
#     def login(self, email, password):
#         self.email = email
#         self.password = password



class Fshare:
    def __init__(self):
        self.session = requests.Session()
        self.redis = r.Redis(host='redis_2', port=6379)

    def getCsrf(content):
        return content.split('name="csrf-token" content="')[1].split('">')[0]
    def login(self, email, password):
        self.email = email
        # self.email = email
        # self.password = password
        response = self.session.get("https://www.fshare.vn/")
        header = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Origin': 'https://www.fshare.vn',
            'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.2 Safari/605.1.15',
            'Referer': 'https://www.fshare.vn/',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8'
        }
        data = {
            '_csrf-app': Fshare.getCsrf(response.text),
            'LoginForm[email]': email,
            'LoginForm[password]': password,
            'LoginForm[rememberMe]': 0,
            'LoginForm[rememberMe]': 1
        }
        response = self.session.post("https://www.fshare.vn/site/login", headers=header, data=data)
        if (response.url != "https://www.fshare.vn/file/manager"):
            return False
        self.redis.set(email, pickle.dumps(self.session.cookies), ex=36000)
        return True
    def update_cookie(self, email):
        self.email = email
        try:
            self.session.cookies.update(pickle.loads(self.redis.get(email)))
            response = self.session.get("https://www.fshare.vn")
            if response.url == "https://www.fshare.vn/file/manager":
                return True
            return False
        except:
            return False
    def getlink(self, code):
        
        header = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Accept-Encoding': 'gzip, deflate',
            'Upgrade-Insecure-Requests': '1'
        }
        link = "https://fshare.vn/file/" + code
        response = self.session.get(url=link, headers=header)
        csrf = Fshare.getCsrf(response.text)
        header = {
        # ':authority': 'www.fshare.vn',
        # ':method': 'POST',
        # ':path': '/download/get',
        # ':scheme': 'https',
        'Accept': '*/*',
        'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8',
        'Connection': 'keep-alive',
        'Origin': 'https://www.fshare.vn',
        'Referer': response.url,
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'none',
        'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1',
        'X-CSRF-Token' : csrf,
        'X-Requested-With': 'XMLHttpRequest',
        # 'Cookie': cookie
        }
        data = {
        '_csrf-app': csrf,
        "linkcode": code,
        "ushare": "",
        "withFcode5": 0
        }
        response = self.session.post('https://www.fshare.vn/download/get', headers=header, data=data)
        self.redis.set(self.email, pickle.dumps(self.session.cookies), ex=36000)
        return response.json()


        
        
